\chapter{闭曲面的分类}

\begin{quote}{\LARGE\fontspec{FZHuangCao-S09S}
天地有正气，杂然赋流形.
\begin{flushright}
  文天祥《正气歌》
\end{flushright}}
\end{quote}
\newpage
\section{拓扑流形}
我们生活的地球本来是一个曲面，但是我们却并没有感觉到它的弯曲. 因为，地球的半径实在太大了，在我们每个
人周围很小的区域内，我们可以近似认为空间是平直的. 也就是说，地球从每个点的局部上来看是同胚于欧式空间
的. 流形这个概念，就是将曲面的这个性质提取出来而得到的. 流形，粗略的说，就是具有局部欧式特性的拓扑空
间.

\begin{defn}
  一个Hausdorff拓扑空间$X$称为$n$维（拓扑）流形，若$X$的任意一点都有一个同胚于$\euclid^n$或者
  $\euclid^n_+$的开邻域. 其中$\euclid^n_+$为上半个$n$维欧式空间
  \begin{displaymath}
    \euclid^n_+ =\{(x_1,x_2,\cdots, x_n)\;|\; x_n\ge 0\}.
  \end{displaymath}
\end{defn}
\begin{eg}
  $n$维欧式空间$\euclid^n$, $n$维球面$S^n$, $n$维单位圆盘$D^n$, $n$维环面$T^n$都是$n$维流形.
\end{eg}
% \begin{eg}
%   M\"obius带，射影平面$\proj^2$，Klein瓶都是二维流形.
% \end{eg}
\begin{ex}
  请问下述曲面是不是二维流形？
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{ch5/surfaces}
    \caption{曲面}
    \label{fig:surfaces}
  \end{figure}
\end{ex}
\begin{defn}
  设$M$为$n$维流形，若点$x\in M$有同胚于$\euclid^n$的开邻域，则称$x$为$M$的内点; 若点$x\in M$有同胚
  于$\euclid^n_+$的开邻域，则称$x$为$M$的边界点. 全体内点的集合称为$M$的内部，它是$M$中的一个开集.
  全体边界点的集合称为$M$的边界，记为$\partial M$. 一般来说$\partial M$ 是一个$(n-1)$维流形.
\end{defn}
\begin{rmk}
  注意，如果$\euclid^n$与$\euclid^m$同胚的话($n\neq m$)，则维数的概念就会失去意
  义. 如果$\euclid^n$与$\euclid^n_+$同胚时，边界的概念就会失去意义. 但是我们现在的工具不足以证明这些
  拓扑空间确实是不同胚的，只能暂时接受这个结论.
\end{rmk}

\section{闭曲面}
\begin{defn}
  曲面就是二维流形. 闭曲面是无边紧致连通的曲面.
\end{defn}
\begin{eg}
  $S^2$, $T^2$, $\proj^2$, Klein瓶都是闭曲面. 但是 $D^2$, M\"obius带, $\euclid^2$, 平环都不是闭曲面.
\end{eg}
\begin{ex}
  证明：射影平面$\proj^2$，$T^2$及Klein瓶是闭曲面.
\end{ex}

\section{两类闭曲面}

在闭曲面的分类中，有两种类型是不可或缺的，这两种都可以通过对球面进行\textbf{手术(Surgery)}得到. 下面
我们将对这两种类型逐一进行介绍.

\begin{enumerate}
\item [一.] 安装环柄的球面.
  \textbf{环柄}就是从环面上挖掉一个小圆盘剩下的空间. 如图所示
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{ch5/torusHandle}
    \caption{环柄}
    \label{fig:torusHandle}
  \end{figure}
  下面我们将在球面上安装环柄，这种手术将帮助我们构造出一系列闭曲面. 在球面上挖去一个小圆盘，在开口处
  粘上一个环柄，这样就得到了\textbf{安装一个环柄的球面}. 显然，这样得到的拓扑空间一定同胚于环面
  $T^2$.
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{ch5/assembleTorusHandleToSphere}
    \caption{安装一个环柄的球面}
    \label{fig:assembleTorusHandleToSphere}
  \end{figure}
  也可以在球面上安装多个环柄，例如下述例子：
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{ch5/assembleMultipleTorusHandleToSphere}
    \caption{安装三个环柄的球面}
    \label{fig:assembleMultipleTorusHandleToSphere}
  \end{figure}
  安装了$n$个环柄的球面叫做$n$-环面，记为$nT^2$. 安装的环柄个数叫做\textbf{亏格(genus)}. 因此$n$-环面
  可以称为亏格为$n$的可定向曲面.

\item [二.] 安装交叉帽的球面
  \textbf{交叉帽}就是M\"obius带. 安装交叉帽的球面就是在球面上挖一个洞，然后沿边界粘接一个M\"obius带.
  由于粘接M\"obius带的过程很难用图画出，因此我们就用下面的阴影表示：
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{ch5/assembleCrosscapToSphere}
    \caption{安装了一个交叉帽的球面.}
    \label{fig:assembleCrosscapToSphere}
  \end{figure}
  如果在球面上粘接$m$个M\"obius带又称为球面上安装了$m$个交叉帽，记为$m\proj^2$，称为亏格为$m$的不可
  定向曲面.

  由上一节的知识可知，$1\proj^2\cong \proj^2$，$2\proj^2\cong$ Klein瓶.
  
\end{enumerate}

\section{闭曲面分类定理}
\begin{thm}\label{thm:classification}
  $S^2$，$\{nT^2\}$和$\{m\proj^2\}$不重复的列出了闭曲面的所有拓扑类型，且$\forall$ $n$,
  $m\in\natural$，当$n\neq m$时，$nT^2\neq m T^2$，$n\proj^2\neq m\proj^2$.
\end{thm}

\begin{rmk}
  从1860年来，大多数拓扑学教材都采用Seifert和Threlfall的证明方法. 但是，值得注意的是，Conway在1992年
  给出了这个问题的另外一种证明方法，他称之为 "Zero Irrelevant Proof"(ZIP), 这种证明大家可以在网上找
  到. 有兴趣的读者不妨读一下.
\end{rmk}
\begin{rmk}
  要证明闭曲面分类定理，我们必须要首先介绍闭曲面的多边形表示. 例如，环面$T^2$可以看成由四边形粘合两组
  对边得来，因此四边形$\Gamma$和粘合映射$\phi$就构成了环面的一种多边形表示,记为$(\Gamma,\phi)$.
\end{rmk}
\begin{rmk}[多边形粘合关系的表示方法]
  为了方便描述，我们将多边形的粘合关系用一串字母来表示:
  \begin{enumerate}
  \item 在多边形上，将需要粘接的边对用同样字母表示，用箭头表示粘合方式；
  \item 以某一点为起始，按逆时针方向标出所有边之方向，在上标位置用$+1$或$-1$表示箭头方向与逆时针方向
    相同或相反.
  \end{enumerate}
  例如，对于我们常见的一些拓扑空间，其多边形粘接关系的表示方法如下：
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{ch5/sixReps}
    \caption{粘接关系的表示方法}
    \label{fig:sixReps}
  \end{figure}
\end{rmk}
对于定理~\ref{thm:classification}的证明，由于知识和篇幅的限制，我们不能给出全部的细节. 例如，我们不
去证明任何一个闭曲面都有多边形表示，也不去证明当$n\neq m$时，$nT^2\neq m T^2$，$n\proj^2\neq
m\proj^2$. 
\begin{lm}
  任何闭曲面都有多边形表示.
\end{lm}
证明略.

\begin{rmk}
  具有相同多边形表示的闭曲面相互是同胚的. 但是，具有不同多边形表示的两个闭曲面也不一定不同胚，很多闭
  曲面都有多种多边形表示. 因此，若想证明定理，我们首先应排除这些不确定因素，考虑闭曲面的所谓“标准的
  多边形表示”.
\end{rmk}
标准多边形表示有如下两类：
\begin{enumerate}
\item [($I_n$)] $a_1b_1a_1^{-1}b_1^{-1}a_2b_2a_2^{-1}b_2^{-1}\cdots a_nb_na_n^{-1}b_n^{-1}$, ($n> 0$)
\item [($II_m$)] $a_1a_1a_2a_2\cdots a_ma_m$, ($m> 0$).
\end{enumerate}
这样，定理~\ref{thm:classification}的证明就转化为证明下述命题了：
\begin{prop}\label{prop:canonicalize}
  除球面外，任意闭曲面均有标准多边形表示，且第一类多边形表示$I_n$对应于$nT^2$，第二种多边形表示
  $II_m$对应于$m\proj^2$.
\end{prop}
为把多边形表示化为标准表示，我们要对多边形表示进行一些手术. 首先介绍一些术语：
\begin{enumerate}
\item 同向对：见图~\ref{fig:sameWayOtherWay};
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.2\textwidth]{ch5/sameWay}\qquad
    \includegraphics[width=0.2\textwidth]{ch5/otherWay}
    \caption{同向对和反向对}
    \label{fig:sameWayOtherWay}
  \end{figure}
\item 反向对：见图~\ref{fig:sameWayOtherWay};
\item 顶点类：在粘合映射$\phi$的作用下，顶点分成若干类，同一类中的顶点将粘合为一个;
\item 手术A：粘合相邻反向对. 进行手术后，多边形边数$-2$，顶点类数$-1$.
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{ch5/operationA}
    \caption{手术A}
    \label{fig:operationA}
  \end{figure}
\item 手术B：将多边形剪开，粘接一组对边. 进行手术后，多边形边数不变，顶点类数也不变.
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{ch5/operationB}
    \caption{手术B}
    \label{fig:operationB}
  \end{figure}

\end{enumerate}
\begin{rmk}
  手术A,B都不改变“有无同向对”的性质.
\end{rmk}
\begin{proof}[证明命题~\ref{prop:canonicalize}：多边形表示的标准化.]
  通过进行若干次手术A和手术B，可以将多边形表示化为标准表示，步骤如下：
  \begin{enumerate}
  \item [{\bf 步骤一}] 减少多边形边数（由于边数的减少与顶点类数的减少是同时发生的，而且这种减少只能通过手
    术A实现，因此当顶点类数减少为$1$时，就意味着边数不可减少了.）

    设多边形表示中顶点类数大于$1$，设$P$是一个顶点类，那么有两种情况：
    \begin{enumerate}
    \item 顶点类$P$中只有一个顶点，则多边形表示只可能形如图~\ref{fig:operationA}中所示的样子，这时候通
      过手术A可消去顶点类$P$，同时使多边形边数减2.\label{item:a}
    \item 顶点类$P$中不只一个顶点，那么多边形表示必然形如图~\ref{fig:step1-2a}中所示. 这时候通过手术
      B, 多边形表示中顶点类$P$中的顶点个数减少了$1$个.\label{item:b}
      \begin{figure}[H]
        \centering
        \includegraphics[width=0.6\textwidth]{ch5/Step1-2a}
        \caption{顶点类$P$所在的边是相邻的}
        \label{fig:step1-2a}
      \end{figure}
    \end{enumerate}
    因此，如果多边形表示中顶点类的个数大于$1$，那么就可以选择一个顶点类$P$，(b)将顶点类中顶点个数降
    为$1$，然后(a)将这个顶点类消去. 使最后多边形表示中只留下唯一的一个顶点类. 这时，{\bf 步骤一}就完成了.
    由于只有手术A才能改变顶点类个数和边数，因此在进行完{\bf 步骤一}后，多边形表示最后的顶点类数是$1$，边数
    为$l-2(k-1)$.

  \item [{\bf 步骤二}]

    \begin{lm}\label{lm:afterStep1}
      在步骤一进行之后，新的多边形表示中，反向边对不相邻，并且至少和另一组对边相间排列.
    \end{lm}
    \begin{proof}[证明.]
      如果反向边对相邻，则这两条边中间的顶点一定自成一个顶点类，因此顶点类个数至少为$2$，不可能. 如
      果相间排列的边中没有一组是要粘合的对边，则这显然这不构成一个闭曲面的多边形表示.
    \end{proof}
    下面，我们对新多边形表示中的同向对情况进行讨论：
    \begin{enumerate}
    \item 若新多边形表示中没有同向对，则我们可以通过手术 B，将不相邻的反向对变成相邻的：
      \begin{figure}[H]
        \centering
        \includegraphics[width=0.8\textwidth]{ch5/Step2-1}
        \caption{无同向对}
        \label{fig:noSameOrientalPair}
      \end{figure}
      这样，经过若干次手术后，我们可以将多边形表示变
      为$$a_1b_1a_1^{-1}b_1^{-1}a_2b_2a_2^{-2}b_2^{-2}\cdots a_nb_na_n^{-1}b_n^{-1},$$由引
      理~\ref{lm:afterStep1}，边数一定是$4$的倍数.
    \item 若新的多边形表示中有同向对，则又可以分为两种情况：
      \begin{enumerate}
      \item 若所有的边对都是同向对：通过手术 B，可将不相邻的同向对变成相邻同向对，如
        图~\ref{fig:allSameOrientalPair}
        \begin{figure}[H]
          \centering
          \includegraphics[width=0.6\textwidth]{ch5/Step2-2a}
          \caption{均为同向对}
          \label{fig:allSameOrientalPair}
        \end{figure}
        这个过程不会改变同向对的个数，因此经过若干次手术后，所有的同向对都会成为相邻的. 最终得到的多
        边形表示为
        \begin{displaymath}
          a_1a_1a_2a_2\cdots a_ma_m.
        \end{displaymath}

      \item 若不但有同向对，还有反向对，则同样通过手术 B，可将所有不相邻的同向对变成相邻同向对. 这时，
        若有一对反向对，与他相间排列的必然是反向对，如图~\ref{fig:notAllSameOrientalPair}所示：
        \begin{figure}[H]
          \centering
          \includegraphics[width=0.6\textwidth]{ch5/Step2-2b}
          \caption{有同向对和反向对}
          \label{fig:notAllSameOrientalPair}
        \end{figure}
        如图所示就会消去两个反向对，变成都是同向对；然后，再将同向对变成相邻，如此循环；最后可以消去
        所有反向对，得到的多边形表示也是
        \begin{displaymath}
          a_1a_1a_2a_2\cdots a_ma_m.
        \end{displaymath}
      \end{enumerate}
    \end{enumerate}
  \end{enumerate}
  至此，我们就完成了闭曲面分类定理的证明.
\end{proof}
\begin{rmk}
  总结：标准多边形表示的边数为$l-2(k-1)$，其中$k$是原多边形表示的顶点类数. 由于手术 A 和 B 都不改变
  同向对的个数，若原多边形表示中有同向对，则所对应的标准多边形表示一定是$II_m$型的；反之，若没有同向
  对，则得到的是$I_n$型的. 因此，任意给出多边形表示后，我们都能很容易的得知其所代表的闭曲面类型.
\end{rmk}

\begin{eg}
  试求$a_1b_1c_1a_1^{-1}b_1c_1^{-1}d_1d_1$所代表的闭曲面类型.
\end{eg}\label{eg:polygon}
\begin{proof}[解.] 多边形表示如下图，顶点类数$k=1$，边数$l=8$，有同向对. 因此标准多边形表示的边数为
  $8-2\times(1-1)=8$，是$II_m$型的. 所以$m=4$. 闭曲面类型为$4\proj^2$
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.3\textwidth]{ch5/eg}
    \caption{例~\ref{eg:polygon}}
    \label{fig:egPolygon}
  \end{figure}
\end{proof}

\newpage
\section*{习题}
\input{ch5/ex12/ex12}