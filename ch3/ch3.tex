\chapter{紧致性与连通性}
\newpage
\section{紧致性与$\euclid^n$中的有界闭集}
紧致性是$\euclid^n$中有界闭集概念在拓扑空间中的一种推广. “有界闭”这个概念依赖于距离的概念，而紧致性
是仅由拓扑空间本身的结构就可以定义的.
\begin{defn}[拓扑空间中的开覆盖] 设$X$是一个拓扑空间，$\mathcal{F}$是$X$中一族开集，满足
  $\mathcal{F}$中全体元素之并为$X$
  \begin{displaymath}
    \bigcup \mathcal{F} = X,
  \end{displaymath}
  我们称$\mathcal{F}$为$X$的开覆盖. 若$\mathcal{F}'$为
  $\mathcal{F}$的子族，但仍有$\mathcal{F}'$中全体元素的并为$X$，则称$\mathcal{F}'$为$\mathcal{F}$的
  子覆盖.
\end{defn}

\begin{eg}
  设$X=\euclid^2$，$\mathcal{F}$为以整数点为圆心，$1$为半径的开圆盘全体构成的集族. 则$\mathcal{F}$是
  $X$的一个开覆盖. 但是$\mathcal{F}$中去掉任何一个开集都不能覆盖$X$.
\end{eg}

\begin{eg}
  设$X=[0,1]\subset\euclid^1$，$X$上具有子空间拓扑. 设
  \begin{displaymath}
    \mathcal{F}= \left\{\left[0,\frac1{10}\right), \left(\frac13,1\right], \left(\frac1{n+2},
        \frac1{n}\right) \;\quad (n\ge 2)\right\}.
  \end{displaymath}
  则$\mathcal{F}$构成了$X$的一个开覆盖. 并且实际上取$n=2,3,\cdots,9$就可以覆盖$X$. 因此有有限子覆盖.
\end{eg}

\begin{defn}[紧致性] 拓扑空间$X$被称为紧致的，如果$X$的任意开覆盖$\mathcal{F}$总有有限子覆盖.
\end{defn}

\begin{rmk}
  由于定义紧致性时仅涉及到了$X$的拓扑结构，因此紧致性是一种拓扑性质.
\end{rmk}

\begin{rmk}
  紧致性都是对拓扑空间本身来说的，并不像数学分析中那样针对某个大空间的子集. 对于大空间中子集的紧致性，
  我们有如下定义.
\end{rmk}

\begin{defn}[紧致子集] 拓扑空间$X$中的子集$C$被称为紧致子集，如果$C$在$X$的子空间拓扑的意义下构成的拓
  扑空间是紧致的.\label{defn:compact-subset}
\end{defn}
\begin{rmk}
  由于$C$中的任何开集都是由大空间中开集与$C$相交得到的，因此定义~\ref{defn:compact-subset}也等价于：
  任何由$X$中开集构造的$C$的开覆盖有有限子覆盖.
\end{rmk}

\begin{thm}[Heine-Borel] 欧式空间$\euclid^n$中的有界闭集都是紧致子集.
\end{thm}

\begin{proof}[证明略.]
\end{proof}
\newpage
\section{紧致拓扑空间的性质}
\begin{thm}
  紧致拓扑空间在连续映射下的像是紧致的.
\end{thm}
\begin{proof}[证明.] 为方便证明，不妨假设$f:X\longrightarrow Y$为连续满射. 其中$X$是紧致拓扑空间，
  $Y$是$f$的像.
  \begin{figure}[ht]
    \centering
    \includegraphics[width=0.7\textwidth]{ch3/compact-image}
    \caption{紧致空间的像紧致}
    \label{fig:cmpt-img}
  \end{figure}
任取$Y$的开覆盖$\mathcal{F}$，设$O\in\mathcal{F}$为任意开集，则由$f$的连续性，$f^{-1}(O)$为$X$中开集.
进而$\mathcal{G}=\{f^{-1}(O)\;|\;O\in\mathcal{F}\}$构成了$X$的开覆盖. 因为$X$是紧致的，所以任意开覆
盖有有限子覆盖，不妨假设$\{f^{-1}(O_i)\;|\; i=1,2,\ldots,n\}$构成了$X$的有限子覆盖. 则显然
$\{O_i\;|\; i=1,2,\ldots,n\}$构成了$Y$的有限子覆盖. 因此$Y$是紧致的.
\end{proof}

\begin{thm}\label{thm:compact-closed-subset}
  紧致空间中的闭子集是紧致的.
\end{thm}
\begin{proof}[证明.]
  假设$X$是紧致拓扑空间，$A$是$X$的一个闭子集. 任取$A$在$X$中的开覆盖，记为$\mathcal{F}$. 即
  $\mathcal{F}$中元素都是$X$中开集，并且
  \begin{displaymath}
    A\subset\bigcup \mathcal{F}.
  \end{displaymath}
  覆盖了$A$.
  \begin{figure}[ht]
    \centering
    \includegraphics[width=0.6\textwidth]{ch3/compact-closed-subset}
    \caption{紧致空间的闭子集紧致}
    \label{fig:cmpt-clsed-subset}
  \end{figure}
  注意到$A$是闭集，$$\mathcal{F}':=\mathcal{F}\cup \{X-A\}$$构成了$X$的一个开覆盖，因此一定有有限子
  覆盖，记此有限子覆盖为$\mathcal{G}$. 若$\mathcal{G}$中包含$X-A$，则剩下的元素一定盖住了$A$，构成了
  $\mathcal{F}$的一个子覆盖；若$\mathcal{G}$中不包含$X-A$，则$\mathcal{G}$本身就是$A$的一个有限子覆
  盖. 因此，$A$是紧致子集.
\end{proof}

\begin{thm} 若$A$为Hausdorff空间$X$中的紧致子集，若$x\in X-A$，则存在两个不交开集$U$和$V$，使得
  $A\subset U$ 且$x\in V$，我们称“$U$和$V$将$A$和$x$分开”. \label{thm:compact-hausdorff}
\end{thm}
\begin{proof}[证明.]
  任取一点$y\in A$，由Hausdorff性质，存在开集$U_y$与$V_y$将$y$与$x$分开. 因此$\{U_y\;|\; y\in A\}$就
  构成了$A$的一组开覆盖. 又由$A$的紧致性，这组开覆盖一定有有限子覆盖，记此子覆盖
  为$\{U_{y_1},\cdots,U_{y_n}\}$. 令$U = \bigcup_i U_{y_i}$，$V=\bigcap_i V_{y_i}$. 易知$U$和$V$是不
  相交的开集，且$A\subset U$，$x\in V$，即所求.
  \begin{figure}[ht]
    \centering
    \includegraphics[width=0.6\textwidth]{ch3/compact-hausdorff}
    \caption{Hausdorff空间中的紧致子集}
    \label{fig:cmpt-hausdorff}
  \end{figure}
\end{proof}

\begin{cor}\label{cor:hausdorff-compact}
  Hausdorff空间中的紧致子集是闭集.
\end{cor}

\begin{proof}[证明.]
  设$A$是Hausdorff空间$X$中的紧致子集. 任取$x\not\in A$，由于定理~\ref{thm:compact-hausdorff}，一定
  存在两个不相交的开集$U_x$和$V_x$，使得$A\subset U_x$，$x\in V_x$. 从而$V_x \subset X-A$. 那么
  \begin{displaymath}
    X-A = \bigcup_{x\in X-A} V_x
  \end{displaymath}
  显然是开集，所以$A$是$X$中闭集.
\end{proof}

\begin{thm}
  从紧致拓扑空间$X$到Hausdorff空间$T$的连续双射是同胚.
\end{thm}
\begin{proof}[证明.]
  只需验证逆映射是否连续. 这等价于验证原映射是不是闭映射. 任取$A$为$X$中闭子集，由于$X$是紧致空间，
  $A$一定是$X$中紧致子集（定理~\ref{thm:compact-closed-subset}）. 由于连续映射将紧集映为紧集，所以
  $A$的像集是$T$中紧集. 而Hausdorff空间中的紧致子集是闭集（推论~\ref{cor:hausdorff-compact}），因此
  原映射将紧集映为紧集.
\end{proof}

\begin{rmk}
  紧致性是一个拓扑性质，因为紧致性是在同胚映射下保持不变.
\end{rmk}

下面，我们将要阐述拓扑空间中的极限点的概念与性质。在欧式空间中，Bolzano-Weierstrass定理告诉我们，有
界无穷集合必有至少一个极限点，这个性质对于一般的拓扑空间也有对应的结论，只不过有界性应该改为紧致性.

\begin{thm}
  紧致空间中的无穷子集必有极限点.
\end{thm}

\begin{proof}[证明.]
  利用反证法. 假设$A$是紧致拓扑空间$X$中的无穷子集，但$A$在$X$中没有极限点. 那么，任给$X$中一点$x$，
  总存在一个开集$U_x$，它和$A$的交至多含有$x$一点. 这样的$\{U_x\}$全体构成了$X$的一组开覆盖，由于$X$
  是紧致的，这组开覆盖必然有有限子覆盖，即存在$\{U_{x_1},\cdots,U_{x_n}\}$覆盖了$X$. 但是
  $U_{x_1},\cdots,U_{x_n}$至多只能覆盖住$A$中有限多个点，这与$A$是无限集矛盾.
\end{proof}

\begin{thm}
  欧式空间中的紧致子集是有界闭集.
\end{thm}

\begin{proof}[证明.]
  欧式空间是Hausdorff空间. 因此用推论~\ref{cor:hausdorff-compact}可以马上知道紧致子集是闭集. 其次，
  用一串中心在原点，半径为$n\in\mathbb{N}$的同心圆盘$B(0,n)$可以覆盖这个紧致子集. 这组覆盖一定有有限
  子覆盖. 因此，这个紧致子集必定落在有限多个同心圆盘$B(0,n)$之内，从而是有界集合.
\end{proof}

\begin{thm}
  紧致空间上的连续实值函数有界，并且能够取得它的界.
\end{thm}

\begin{proof}[证明.]
  紧致空间上的连续实值函数的像是$\euclid^1$中的紧集，从而是有界闭集. 有界闭集的上下确界都在集合内，
  因此能够被达到.
\end{proof}

\begin{lm}[Lebesgue引理]
  设$X$是一个紧致的度量空间，$\mathcal{F}$是$X$的一个开覆盖，则存在一个实数$\delta>0$（称为
  $\mathcal{F}$的Lebesgue数），使得$X$中任意直径小于$\delta$的集合必然包含在某个$\mathcal{F}$的成员
  中.
\end{lm}
\begin{proof}[证明.]
  反证法. 假设这样的$\delta$不存在. 则在
\end{proof}

\newpage
\section{乘积拓扑空间}

\subsection{拓扑基回顾}
\begin{thm} 设 $\beta$ 是由$X$的某些子集构成的一个非空集族. 如果$\beta$内有限多个成员之并仍在$\beta$
  中，则$\beta$可作为$X$上某个拓扑结构的拓扑基.
\end{thm}

\begin{proof}[证明.]
  考虑将$\beta$中所有可能元素之并均作为开集. 令$\mathcal{T}=\{U\;|\; U\text{是}\beta\text{中若干元素
    之并}\}$，则显然
  \begin{enumerate}
  \item [$1^\circ$] $\emptyset$，$X$ $\in\mathcal{T}$；
  \item [$2^\circ$] $\bigcup_{\alpha\in I} U_\alpha \in \mathcal{T}$；
  \item [$3^\circ$] $
    U \cap V = \left(\bigcup_{\alpha\in I} B_\alpha\right)\cap \left(\bigcup_{\gamma\in
        J}C_\gamma\right)
    =\bigcup_{\alpha\in I}\bigcup_{\gamma\in J} \left(B_\alpha \cap C_\gamma\right)\in\mathcal{T}$.
  \end{enumerate}
\end{proof}

\subsection{乘积集合上的拓扑结构}

\begin{eg}乘积集合的例子.
  \begin{enumerate}
  \item [(1)] $\real^2 = \real \times \real$;
    \begin{figure}[ht]
      \centering
      \includegraphics[width=0.4\textwidth]{ch3/R2timesR2}
      \caption{$\real^2 = \real\times \real$}
      \includegraphics[width=0.4\textwidth]{ch3/cylinder.png}
      \caption{圆柱面$=S^1\times I$}
      \label{fig:plane}
    \end{figure}

  \item [(2)] 圆柱 $=S^1\times I$, 其中$S^1$ 表示二维空间中的单位圆周, $I=[0,1]$ 表示单位闭区间;

  \item [(3)] 环面 $= S^1 \times S^1$.
    \begin{figure}[ht]
      \centering
      \includegraphics[width=0.4\textwidth]{ch3/torus.png}
      \caption{环面 $= S^1 \times S^1$}
      \label{fig:torus}
    \end{figure}
  \end{enumerate}
\end{eg}

\begin{defn}
  设$X$和$Y$为两个拓扑空间. 令乘积集合$X\times Y$上的一个拓扑结构定义为
  \begin{displaymath}
    \mathcal{T} := \left\{\bigcup_{\alpha\in I}\left(U_\alpha \times V_\alpha\right) \;\bigg| \;
      U_\alpha\subset X \text{为}X\text{中开集},\; V_\alpha\subset Y\text{为}Y\text{中开集}.\right\}
  \end{displaymath}
  则称$\mathcal{T}$为乘积拓扑. 
  \begin{displaymath}
    \mathcal{B} := \left\{U \times V \;\bigg| \;
      U\subset X \text{为}X\text{中开集},\; V\subset Y\text{为}Y\text{中开集}.\right\}
  \end{displaymath}
  为$\mathcal{T}$的一组拓扑基. $(X\times Y, \mathcal{T})$就称为乘积拓扑空间.
\end{defn}
\begin{rmk}
  对于$n$个拓扑空间的乘积空间, 我们可以归纳定义，比如$X_1\times X_2\times \cdots \times X_n :=
  (X_1\times \cdots \times X_{n-1})\times X_n$.
\end{rmk}

\begin{eg} 二维欧式空间$\euclid^2$上的拓扑结构与$\euclid\times\euclid$上的乘积拓扑相同.
\end{eg}
\begin{proof}[证明.]
  任取欧式空间$\euclid^2$中的开集$O$. 对于任意一点$(x,y)\in O$，总能找到一个包含这个点$(x,y)$的长方形
  开集$U_x\times V_y$，使得$U_x\times V_y\subset O$. 因此，$$O=\bigcup_{(x,y)\in O}U_x\times V_y.$$
  这说明$\mathcal{B}=\{U\times V\;|\; U\subset X, V\subset Y\}$ 是$\euclid^2$的一组拓扑基. 而
  $\mathcal{B}$也是乘积拓扑的拓扑基，因此两拓扑结构相同.
\end{proof}

\begin{defn}
  由乘积拓扑空间$X\times Y$至$X$或$Y$的自然投影映射：
  \begin{align*}
    &p_X : X\times Y \longrightarrow X,\quad\quad &p_y:X\times Y\longrightarrow Y\\
    & p_X: (x,y) \mapsto x & p_Y:(x,y) \mapsto y.
  \end{align*}
\end{defn}

\begin{thm}
  若$X\times Y$ 上具有乘积拓扑，则投影映射$p_X$和$p_Y$均为连续开映射. 并且乘积拓扑是使$p_X$和$p_Y$都
  连续的最小拓扑结构.
\end{thm}

\begin{proof}[证明.]
  只需讨论$p_X$即可. 对于$X$中的任意开集$U$，$U$对$p_X$的原像为$X\times Y$中集合$U\times Y$，显然这
  是$X\times Y$ 中的一个开集，所以$p_X$是连续映射. 而要证明$p_X$是一个开映射，只需对于$X\times Y$中的
  任意\textbf {基本开集}$U\times V$，证明其像是$X$中开集即可（请读者思考这是为什么）.而$p_X(U\times
  V) = U$ 是$X$中开集显然.

  对于定理的后半部分，我们需证明对于任意$X\times Y$上的拓扑结构$\mathcal{T}'$来说，要使$p_X$和$p_Y$
  都连续，它必然要包含乘积拓扑$\mathcal{T}$. 要使$p_X$连续，必然要使$p_X^{-1}(U)=U\times Y$成为
  $X\times Y$中开集. 同理，要使$p_Y$连续，必然要使$p_Y^{-1}(V)=X\times V$成为$X\times Y$中开集. 从而，
  $U\times V$也必然要成为$X\times Y$中的开集. 所以$\mathcal{T}'$中必须要包含乘积拓扑的拓扑基
  $\mathcal{B}$，所以$\mathcal{T}\subset \mathcal{T}'$. 因此证明了$\mathcal{T}$的最小性.
\end{proof}

我们在《数学分析》中考虑向量值函数的时候经常会说，向量值函数连续的充要条件就是它的每个分量函数都连续.
在研究取值在乘积空间上的连续映射时，这个结论仍然是成立的，我们将其归结为下面的定理.

\begin{thm} 映射$f:Z\longrightarrow X\times Y$连续，当且仅当$p_X\circ f$和$p_Y\circ f$均连续.
\end{thm}
\begin{proof}[证明.]
  如果$f$连续，由复合函数的连续性立即可知$p_X\circ f$和$p_Y\circ f$均连续.  反之，如果$p_X\circ
  f$和$p_Y\circ f$分别是由$Z$至$X$和$Y$的连续映射. 那么对于$X\times Y$中任何一个基本开集$U\times
  V$，$f^{-1}(U\times V)=(p_X\circ f)^{-1}(U)\cup (p_Y\circ f)^{-1}(V)$ 也是$Z$中开集.
\end{proof}

\begin{thm}
  若$X\times Y$ 为 Hausdorff 空间，则当且仅当$X$和$Y$均为 Hausdorff 空间.
\end{thm}

\begin{proof}[证明.]
  
\end{proof}

\begin{lm}
  设$X$为拓扑空间，设$\mathcal{B}$ 为$X$的一组拓扑基，则$X$为紧致拓扑空间，当且仅当$\mathcal{B}$中成
  员组成的$X$的任意开覆盖都有有限字覆盖.
\end{lm}
\begin{proof}[证明.]
  
\end{proof}

\begin{thm}
  $X\times Y$为紧致的拓扑空间，当且仅当$X$与$Y$均是紧致的拓扑空间.
\end{thm}
\begin{proof}[证明.]
  
\end{proof}

\begin{rmk}
  实际上任意多个紧致拓扑空间的乘积都是紧的. 这就是 Tychonoff定理. 这个定理的证明已经超出了本讲义的范
  围，请感兴趣的读者自己寻找答案.
\end{rmk}

\newpage
\section{连通性}

直观上说，一个几何对象是连通的，就是说它只由一个部分组成. 直线是连通的，因为若将直线分为互不相交的两
部分，那么这两个部分必然“紧挨着”. 那么，由拓扑观点如何描述这一性质呢？我们知道，拓扑空间上可以没有度
量，唯一确定具有的就是开集和闭集的概念. 因此，拓扑中要描述连通性，必然要借助和开集、闭集有关的概念.
比如，$[0,1]$闭区间可分解为$[0,1/2)$ 和$[1/2,1]$. 由拓扑性质可知，第一部分的闭包和第二部分一定相
交. 由此，我们给出了拓扑空间连通的明确定义:
\begin{defn}
  拓扑空间$X$是连通的，如果$X$无法分解为两个非空集合$A$和$B$的不交并，并且$\overline{A}\cap
  B$和$A\cap \overline{B}$均为空集.
\end{defn}
\begin{rmk}
  因为涉及到闭包的概念，所以连通性是一个拓扑性质，和集合上的拓扑结构是紧密相关的. 即使集合相同，只要
  上面赋予的拓扑结构不同，连通性也有可能发生变化. 接下来，我们要证明第一个基本结论：
\end{rmk}
\begin{thm}
  一维欧式空间$\euclid^1$是连通的.
\end{thm}
\begin{proof}[证明.]
  
\end{proof}

\begin{defn}
  集合$A\subset X$称为拓扑空间$X$中的连通子集，如果$A$在子空间拓扑下构成连通的拓扑空间.
\end{defn}

\begin{defn}[$\real$中的区间] 实数集$\real$中某子集$A$称作区间，如果对于任意$a<b\in A$，$a$,$b$之间
  的一切点也要属于$A$.  
\end{defn}

\begin{thm}
  $\euclid^1$中任何非空连通子集一定是区间.
\end{thm}

\newpage

\section*{习题}
\input{ch3/ex6/ex6.tex}
\input{ch3/ex7/ex7.tex}
\input{ch3/ex8/ex8.tex}
\input{ch3/ex9/ex9.tex}
