if(!settings.multipleView) settings.batchView=false;
settings.tex="xelatex";
settings.inlinetex=true;
deletepreamble();
defaultfilename="main-1";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

/* 动画：表现定端同伦 */
import animate;

usepackage("amsmath");
real labelscalefactor = 0.5; /* changes label-to-point distance */
pen dps = linewidth(0.7) + fontsize(10);
defaultpen(dps); /* default pen style */
real xmin = -11, xmax = 5, ymin = -5, ymax = 6; /* 图像大小 */
pen cccccc = rgb(0.8,0.8,0.8);
pen zzzzzz = rgb(0.6,0.6,0.6);
pen fftttt = rgb(1,0.2,0.2);
pair P = (0,0);

pair[] P1={(1,1),(3,1),(2,0.5)};
pair[] P2={(-0.5,2),(1,2),(1.5,1)};

pair[] R = {(-10.26,3.02), (-10.26,-0.98), (-6.26,-0.98), (-6.26,3.02)};

/* 动画部分 */
animation Ani;
int n=40;
for(int i=0;i<n;++i){
picture pic;
size(pic,16cm,11cm);

real t=i/n;
pair A=interp(P1[0],P2[0],t);
pair B=interp(P1[1],P2[1],t);
pair C=interp(P1[2],P2[2],t);

pair E=interp(R[1],R[0],t);
pair F=interp(R[2],R[3],t);

/* 画两个长方形区域 */
draw(pic, (-3,4)--(4,4)--(4,-2)--(-3,-2)--cycle, linewidth(0.4) + cccccc);
draw(pic, (-10.26,3.02)--(-10.26,-0.98)--(-6.26,-0.98)--(-6.26,3.02)--cycle, linewidth(0.4) + cccccc);

/* 画启终道路的图像 */
draw(pic, P..P1[0]..P1[1]..P1[2]..cycle, dotted + zzzzzz);
draw(pic, P..P2[0]..P2[1]..P2[2]..cycle, dotted + zzzzzz);

/* 画活动的道路图像 */
draw(pic, P..A..B..C..cycle, linewidth(2) + fftttt);
draw(pic, E--F, linewidth(2) + fftttt);

/* 画两个矩形之间的箭头 */
draw(pic, (-6.5,1)--(-2,1));
draw(pic, (-4.04,1)--(-4.15,0.87));
draw(pic, (-4.04,1)--(-4.15,1.14));
draw(pic, (-4.25,1)--(-4.36,0.87));
draw(pic, (-4.25,1)--(-4.36,1.14));

/* 写文字 */
label(pic, "$ F: I\times I \longrightarrow X $",(-4.3,1.5),N*labelscalefactor);
label(pic, "$ I=[0,1] $",interp(R[1],R[2],0.5),S*labelscalefactor);

/* 画点 */
dot(pic, P,blue);
label(pic, "$P$", (0.08,0.12), NE * labelscalefactor,blue);
/*dot(pic, (-10.28,-1)); */
label(pic, "$t$ ", E, SW * labelscalefactor);
Ani.add(pic);
}
label(Ani.pdf("controls,loop",keep=true,delay=200));
viewportsize=(472.03123pt,0);
